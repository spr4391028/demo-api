export const AppConstant = {
  PendingFaceCol: 'pendingFace',
  VerifiedFaceCol: 'verifiedFace',
  PendingIDFaceCol: 'pendingIDFace',
  VerifiedIDFaceCol: 'verifiedIDFace',
  DraftFaceCol: 'draftFace',
};
