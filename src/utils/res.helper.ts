export class CError extends Error {
  data: any;
  constructor(message: string, data?: any) {
    super(message);
    this.name = 'ClientError';
    this.message = message;
    this.data = data;
  }
}
export function errorHandler(error: any) {
  if (error instanceof CError) {
    console.log(error);
    return {
      code: 0,
      message: error.message,
      error: {
        name: error.name,
        detail: error.message,
      },
      data: error.data ? error.data : null,
    };
  } else {
    console.error(error);
    return {
      code: -1,
      message: error.message,
      error: {
        name: error.name,
        detail: error.message,
      },
      data: null,
    };
  }
}

export function resHandler(result: any, message?: string, code?: number) {
  return {
    code: code ? code : 1,
    message: message ? message : '',
    data: result,
    error: null,
  };
}

export function toResponse(arg: {
  page: number;
  count: number;
  total_items: number;
  docs: any[];
}): any {
  const { page, count, total_items, docs } = arg;
  const pagination: any = {};
  const totalPages = Math.ceil(total_items / count);
  const hasNextPage = page * count < total_items;
  const hasPrevPage = page - 1 > 0;
  const nextPage = hasNextPage ? page + 1 : null;
  const prevPage = hasPrevPage ? page - 1 : null;
  pagination.total_items = total_items;
  pagination.total_pages = totalPages;
  pagination.has_next_page = hasNextPage;
  pagination.next_page = nextPage;
  pagination.has_prev_page = hasPrevPage;
  pagination.prev_page = prevPage;
  pagination.count = count;
  pagination.page = page;
  return { data: docs, pagination };
}
