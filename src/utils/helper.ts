import { CError } from "./res.helper";
import { createWriteStream, ReadStream } from "fs";
import { isValidObjectId } from "mongoose";

export const helper = {
  fromReadStreamToPathFile: async (stream: ReadStream) => {
    const path = "/tmp/test" + Date.now().toString();
    const myFile = createWriteStream(path);
    const done = await new Promise((resolve, reject) => {
      const result = stream.pipe(myFile);
      result.on("error", () => {
        reject(false);
      });
      result.on("finish", () => {
        resolve(true);
      });
    });
    if (done) {
      return path;
    }
    return null;
  },
  getImageUrl: (id: string) => {
    if (id) {
      const url = "";
      return url + "/files/" + id;
    }
    return null;
  },
  checkValidId: (id: string) => {
    const isValid = isValidObjectId(id);
    if (!isValid) {
      throw new CError("ID is not valid.");
    }
  },
  autoGenerateCode: () => {
    const randomLetter = String.fromCharCode(
      Math.floor(Math.random() * 26) + 65
    );
    const code = Date.now().toString() + "-" + randomLetter;
    return code;
  },
  generateFullName: (index: number) => {
    const family_name_kh = "ត្រកូល";
    const given_name_kh = "នាមខ្លួន";
    const family_name_en = "Trokol";
    const given_name_en = "Nemkloun";
    return {
      family_name_kh: family_name_kh + index,
      given_name_kh: given_name_kh + index,
      family_name_en: family_name_en + index,
      given_name_en: given_name_en + index,
    };
  },
};
