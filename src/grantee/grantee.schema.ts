import { Prop, Schema, SchemaFactory } from "@nestjs/mongoose";
import { Document } from "mongoose";
export class Address {
  country: string;
  province: string;
  district: string;
  commune: string;
  village: string;
  street: string;
  home_number: string;
  group?: string;
  postal_code: string;
  text: string;
}

export type GranteeDocument = Grantee & Document;

@Schema({ timestamps: true })
export class Grantee {
  _id: string;

  @Prop({ type: String, default: null })
  spid: string;

  @Prop({ type: String, default: null })
  beneficiary_id: string;

  @Prop({ type: String, default: null })
  national_id: string;

  @Prop({ type: String, default: null })
  photo: string;

  @Prop({ type: String, default: null })
  family_name_kh: string;

  @Prop({ type: String, default: null })
  given_name_kh: string;

  @Prop({ type: String, default: null })
  family_name_en: string;

  @Prop({ type: String, default: null })
  given_name_en: string;

  @Prop({ type: String, default: null })
  gender: string;

  @Prop({ type: Address, default: null })
  place_of_birth: Address;

  @Prop({ type: Address, default: null })
  current_address: Address;

  @Prop({ type: String, default: null })
  phone_number: string;

  @Prop({ type: String, default: null })
  occupation: string;

  @Prop({ type: Date, default: null })
  issued_date: string;

  @Prop({ type: Date, default: null })
  expired_date: string;

  @Prop({ type: String, default: null })
  email: string;

  @Prop({ type: String, default: null })
  education_level: string;

  @Prop({ type: String, default: null })
  date_of_birth: string;

  @Prop({ type: Object, default: null })
  household_info: any;

  @Prop({ type: Object, default: null })
  failed_result: any;

  @Prop({
    type: String,
    default: "active",
    enum: ["active", "inactive", "failed"],
  })
  status: string;
}

export const GranteeSchema = SchemaFactory.createForClass(Grantee);
