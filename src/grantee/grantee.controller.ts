import {
  GranteeQueryDto,
  CreateGranteeDto,
  UpdateGranteeDto,
} from "./grantee.dto";
import {
  Body,
  Controller,
  Delete,
  Get,
  Logger,
  Param,
  Patch,
  Post,
  Query,
  UseGuards,
} from "@nestjs/common";
import { ApiBearerAuth, ApiOperation, ApiTags } from "@nestjs/swagger";
import { JwtAuthGuard } from "../auth/guards/jwt.guard";
import { RolesGuard } from "../auth/guards/roles.guard";
import { Role } from "../auth/role.enum";
import { GranteeService } from "./grantee.service";
import { MResponse } from "src/commons/custom.type";
import { errorHandler, resHandler } from "src/utils/res.helper";
import { Roles } from "src/auth/decorators/roles.decorator";

@ApiBearerAuth()
@ApiTags("grantees")
@Controller("grantees")
export class GranteeController {
  private logger = new Logger(GranteeController.name);
  constructor(private readonly service: GranteeService) {}

  @Post()
  @ApiOperation({ summary: "Create a grantee" })
  @Roles(Role.Admin, Role.Manager)
  // @UseGuards(JwtAuthGuard, RolesGuard)
  async createGrantee(@Body() data: CreateGranteeDto): Promise<MResponse> {
    this.logger.log(this.createGrantee.name);
    try {
      const result = await this.service.create(data);
      return resHandler(result, this.createGrantee.name);
    } catch (error) {
      return errorHandler(error);
    }
  }

  @Get("/generate-mock-data")
  @ApiOperation({ summary: "Get list of grantee" })
  @Roles(Role.All)
  // @UseGuards(JwtAuthGuard, RolesGuard)
  async generateMockData(): Promise<MResponse> {
    this.logger.log(this.generateMockData.name);
    try {
      const result = await this.service.generateMockData();
      return resHandler(result, this.generateMockData.name);
    } catch (error) {
      return errorHandler(error);
    }
  }

  @Get("/start-sync")
  @ApiOperation({ summary: "Get list of grantee" })
  @Roles(Role.All)
  // @UseGuards(JwtAuthGuard, RolesGuard)
  async startSync(): Promise<MResponse> {
    this.logger.log(this.startSync.name);
    try {
      const result = await this.service.startSync();
      return resHandler(result, this.startSync.name);
    } catch (error) {
      return errorHandler(error);
    }
  }

  @Get()
  @ApiOperation({ summary: "Get list of grantee" })
  @Roles(Role.All)
  // @UseGuards(JwtAuthGuard, RolesGuard)
  async getAllGrantees(@Query() query: GranteeQueryDto): Promise<MResponse> {
    this.logger.log(this.getAllGrantees.name);
    try {
      const result = await this.service.findAll(query);
      return resHandler(result, this.getAllGrantees.name);
    } catch (error) {
      return errorHandler(error);
    }
  }

  @Get(":id")
  @ApiOperation({ summary: "Get one grantee" })
  @Roles(Role.All)
  // @UseGuards(JwtAuthGuard, RolesGuard)
  async getOneGrantee(@Param("id") id: string): Promise<MResponse> {
    this.logger.log(this.getOneGrantee.name);
    try {
      const result = await this.service.findOne(id);
      return resHandler(result, this.getOneGrantee.name);
    } catch (error) {
      return errorHandler(error);
    }
  }

  @Patch(":id")
  @ApiOperation({ summary: "Update a grantee" })
  @Roles(Role.All)
  // @UseGuards(JwtAuthGuard, RolesGuard)
  async updateGrantee(
    @Param("id") id: string,
    @Body() body: UpdateGranteeDto
  ): Promise<MResponse> {
    this.logger.log(this.updateGrantee.name);
    // console.log('grantee', data);
    try {
      const result = await this.service.update(id, body);
      return resHandler(result, this.updateGrantee.name);
    } catch (error) {
      return errorHandler(error);
    }
  }

  @Delete("/clean")
  @ApiOperation({ summary: "Delete a grantee" })
  @Roles(Role.Admin, Role.Manager)
  // @UseGuards(JwtAuthGuard, RolesGuard)
  async cleanData(): Promise<MResponse> {
    this.logger.log(this.cleanData.name);
    try {
      const result = await this.service.cleanData();
      return resHandler(result, this.cleanData.name);
    } catch (error) {
      return errorHandler(error);
    }
  }

  @Delete(":id")
  @ApiOperation({ summary: "Delete a grantee" })
  @Roles(Role.Admin, Role.Manager)
  @UseGuards(JwtAuthGuard, RolesGuard)
  async deleteGrantee(@Param("id") id: string): Promise<MResponse> {
    this.logger.log(this.deleteGrantee.name);
    try {
      const result = await this.service.remove(id);
      return resHandler(result, this.deleteGrantee.name);
    } catch (error) {
      return errorHandler(error);
    }
  }
}
