import { Injectable, Logger } from "@nestjs/common";
import { Model } from "mongoose";
import { toResponse } from "../utils/res.helper";
import { Grantee, GranteeDocument } from "./grantee.schema";
import {
  CreateGranteeDto,
  UpdateGranteeDto,
  GranteeQueryDto,
  RegistrationDto,
  HouseholdInfoDto,
} from "./grantee.dto";
import { InjectModel } from "@nestjs/mongoose";
import { InjectAmqpConnection } from "nestjs-amqp";
import { Channel, Connection, ConsumeMessage } from "amqplib";
import { helper } from "src/utils/helper";
import * as moment from "moment";
import { nanoid } from "nanoid";
import { ConfigService } from "@nestjs/config";

@Injectable()
export class GranteeService {
  private logger = new Logger(GranteeService.name);
  private channel: Channel;
  private OPERATOR_ID;
  private SCHEME_ID;
  private REGISTRATION_QUEUE;
  private REGISTRATION_RESULT_QUEUE;
  private UPDATE_PERSON_QUEUE;
  private UPDATE_PERSON_RESULT_QUEUE;
  constructor(
    @InjectModel(Grantee.name) private model: Model<GranteeDocument>,
    private configService: ConfigService,
    @InjectAmqpConnection("rabbitMq")
    private readonly amqp: Connection
  ) {
    this.OPERATOR_ID = this.configService.get("OPERATOR_ID");
    this.SCHEME_ID = this.configService.get("SCHEME_ID");
    this.REGISTRATION_QUEUE = this.configService.get("REGISTRATION_QUEUE");
    this.UPDATE_PERSON_QUEUE = this.configService.get("UPDATE_PERSON_QUEUE");
    this.UPDATE_PERSON_RESULT_QUEUE = this.configService.get(
      "UPDATE_PERSON_RESULT_QUEUE"
    );
    this.REGISTRATION_RESULT_QUEUE = this.configService.get(
      "REGISTRATION_RESULT_QUEUE"
    );

    this.amqp.createChannel().then((channel) => {
      // assign channel to this service
      this.channel = channel;
      this.channel.prefetch(1);

      // step2: handling registration result
      this.channel.assertQueue(this.REGISTRATION_RESULT_QUEUE);
      this.channel.consume(
        this.REGISTRATION_RESULT_QUEUE,
        (msg) => {
          this.handleMsg(msg);
        },
        { noAck: false }
      );

      // step3: handle update person result
      this.channel.assertQueue(this.UPDATE_PERSON_RESULT_QUEUE);
      this.channel.consume(
        this.UPDATE_PERSON_RESULT_QUEUE,
        (msg) => {
          this.handleMsgUpdate(msg);
        },
        { noAck: false }
      );
    });
  }

  async handleMsg(msg: ConsumeMessage) {
    this.logger.log(this.handleMsg.name);
    const queue = msg.fields.routingKey;
    console.log({ queue });
    try {
      const payload = JSON.parse(msg.content.toString());
      console.log({ payload });
      await this.updateSpid(payload.beneficiary_id, payload.spid);
      this.channel.ack(msg);
    } catch (error) {
      console.log(error);
    }
  }

  async handleMsgUpdate(msg: ConsumeMessage) {
    this.logger.log(this.handleMsg.name);
    const queue = msg.fields.routingKey;
    console.log({ queue });
    try {
      const payload = JSON.parse(msg.content.toString());
      console.log({ payload });
      if (payload.type == "new") {
        const data = payload.data;
        await this.updateSpid(data.beneficiary_id, data.spid);
      }
      this.channel.ack(msg);
    } catch (error) {
      console.log(error);
    }
  }

  async handleErrorMsg(msg: ConsumeMessage) {
    this.logger.log(this.handleErrorMsg.name);
    try {
      const payload = JSON.parse(msg.content.toString());
      console.log({ payload });
      this.model
        .findOneAndUpdate(
          { beneficiary_id: payload.beneficiary_id },
          { $set: { status: "failed", failed_result: payload } }
        )
        .exec();
    } catch (error) {
      console.log(error);
    }
  }

  async generateMockData() {
    this.logger.log(this.generateMockData.name);
    const sampleHousehold: HouseholdInfoDto[] = [
      {
        household_id: "0001",
        member_number: "1",
        male_count: 2,
        female_count: 3,
        member_position: 1,
        is_head: true,
        equity_card_number: "123456789",
        poverty_score: 0,
        is_hiv_affected: false,
        is_covid_affected: false,
        poverty_status: "poor",
        account_number: "123456789",
      },
      {
        household_id: "0002",
        member_number: "1",
        male_count: 2,
        female_count: 3,
        member_position: 1,
        is_head: true,
        equity_card_number: "123456789",
        poverty_score: 0,
        is_hiv_affected: false,
        is_covid_affected: false,
        poverty_status: "poor",
        account_number: "123456789",
      },
      {
        household_id: "0003",
        member_number: "1",
        male_count: 2,
        female_count: 3,
        member_position: 1,
        is_head: true,
        equity_card_number: "123456789",
        poverty_score: 0,
        is_hiv_affected: false,
        is_covid_affected: false,
        poverty_status: "poor",
        account_number: "123456789",
      },
      {
        household_id: "0004",
        member_number: "1",
        male_count: 2,
        female_count: 3,
        member_position: 1,
        is_head: true,
        equity_card_number: "123456789",
        poverty_score: 0,
        is_hiv_affected: false,
        is_covid_affected: false,
        poverty_status: "poor",
        account_number: "123456789",
      },
      {
        household_id: "0005",
        member_number: "1",
        male_count: 2,
        female_count: 3,
        member_position: 1,
        is_head: true,
        equity_card_number: "123456789",
        poverty_score: 0,
        is_hiv_affected: false,
        is_covid_affected: false,
        poverty_status: "poor",
        account_number: "123456789",
      },
    ];
    const count = await this.model.countDocuments();
    console.log({ count });
    const endIndex = count + 20;
    const items: RegistrationDto[] = [];
    for (let i = count; i < endIndex; i++) {
      const registration = new RegistrationDto();
      registration.national_id = (i + 1).toString().padStart(9, "0");
      registration.beneficiary_id = nanoid(12);
      const { family_name_en, given_name_en, family_name_kh, given_name_kh } =
        helper.generateFullName(i);
      registration.family_name_kh = family_name_kh;
      registration.given_name_kh = given_name_kh;
      registration.family_name_en = family_name_en;
      registration.given_name_en = given_name_en;
      registration.gender = i % 2 ? "M" : "F";
      registration.date_of_birth = moment()
        .subtract(20, "years")
        .add(i % 365, "days")
        .format("YYYY-MM-DD");
      registration.place_of_birth = {
        province: "08",
        district: "0810",
        commune: "081005",
        village: "08100505",
      };
      registration.current_address = {
        province: "08",
        district: "0810",
        commune: "081005",
        village: "08100505",
      };
      registration.issued_date = moment().toISOString();
      registration.expired_date = moment().add(1, "years").toISOString();
      registration.phone_number = "+85589609044";
      registration.operator = this.OPERATOR_ID;
      registration.scheme = this.SCHEME_ID;
      registration.email = "test@gmail.com";
      registration.education_level = "undergraduate";
      registration.occupation = "student";
      const index = i % 5;
      const household = sampleHousehold[index];
      const isHead = i < 5 ? true : false;
      household.member_number = (i + 1).toString();
      household.member_position = i + 1;

      registration.household_info = {
        household_id: household.household_id,
        member_number: household.member_number,
        male_count: household.male_count,
        female_count: household.female_count,
        member_position: household.member_position,
        is_head: isHead,
        equity_card_number: household.equity_card_number,
        poverty_score: household.poverty_score,
        is_hiv_affected: household.is_hiv_affected,
        is_covid_affected: household.is_covid_affected,
        poverty_status: household.poverty_status,
        account_number: household.account_number,
      };
      items.push(registration);
    }
    this.model.insertMany(items);

    return { success: true };
  }

  async startSync() {
    this.logger.log(this.startSync.name);
    const people = await this.model.find({ spid: null });
    people.forEach((person) => {
      const registration = person.toObject();
      registration.operator = this.OPERATOR_ID;
      registration.scheme = this.SCHEME_ID;
      // sending data to SPR
      const success = this.channel.sendToQueue(
        this.REGISTRATION_QUEUE,
        Buffer.from(JSON.stringify(registration))
      );
      console.log({ success });
    });
  }

  async updateSpid(benId: string, spid: string) {
    this.logger.log(this.updateSpid.name);
    const result = await this.model.findOneAndUpdate(
      { beneficiary_id: benId },
      { $set: { spid } },
      { new: true }
    );
    if (result) {
      console.log({ spid: result.spid, ben_id: result.beneficiary_id });
    }
  }

  async create(grantee: CreateGranteeDto): Promise<Grantee> {
    this.logger.log(this.create.name);
    const existingData = await this.model.findOne({
      national_id: grantee.national_id,
    });
    if (existingData) {
      throw new Error("This person already registered in the system");
    }
    grantee.beneficiary_id = helper.autoGenerateCode();
    const result = await this.model.create(grantee);
    if (result) {
      if (!result.spid) {
        const registration = new RegistrationDto();
        registration.national_id = result.national_id;
        registration.beneficiary_id = result.beneficiary_id;
        registration.family_name_kh = result.family_name_kh;
        registration.given_name_kh = result.given_name_kh;
        registration.family_name_en = result.family_name_en;
        registration.given_name_en = result.given_name_en;
        registration.gender = result.gender;
        registration.date_of_birth = result.date_of_birth;
        registration.place_of_birth = result.place_of_birth;
        registration.current_address = result.current_address;
        registration.issued_date = result.issued_date;
        registration.expired_date = result.expired_date;
        registration.phone_number = result.phone_number;
        registration.operator = this.OPERATOR_ID;
        registration.scheme = this.SCHEME_ID;
        registration.photo = result.photo ?? undefined;
        registration.email = result.email;
        registration.education_level = result.education_level;
        registration.occupation = result.occupation;
        // sending data to SPR
        const success = this.channel.sendToQueue(
          this.REGISTRATION_QUEUE,
          Buffer.from(JSON.stringify(registration))
        );
        console.log({ success });
      }
    }
    return result;
  }

  async update(id: string, grantee: UpdateGranteeDto): Promise<Grantee> {
    this.logger.log(this.update.name);
    const result = await this.model.findOneAndUpdate(
      { _id: id },
      {
        $set: grantee,
      },
      {
        new: true,
      }
    );
    if (result) {
      if (result.spid) {
        // sending updates data to SPR
        const resultQueue = this.channel.sendToQueue(
          this.UPDATE_PERSON_QUEUE,
          Buffer.from(
            JSON.stringify({
              spid: result.spid,
              operator: this.OPERATOR_ID,
              ...grantee,
            })
          )
        );
        console.log({ resultQueue: resultQueue });
      }
    }
    return result;
  }

  async findAll(arg: GranteeQueryDto): Promise<any> {
    this.logger.log(this.findAll.name);
    let { page, count } = arg;
    const search = arg.search;
    if (!page) page = 1;
    if (!count) count = 10;
    const skip = count * (page - 1);
    const limit = count;
    const query: any = {};
    if (search) {
      query.ben_id = {
        $regex: search,
        $options: "i",
      };
    }
    const total_items = await this.model.countDocuments();
    const docs = await this.model
      .find(query)
      .select("-photo")
      .sort({ _id: -1 })
      .limit(limit)
      .skip(skip);
    return toResponse({ page, count, total_items, docs });
  }

  async findOne(id: string): Promise<Grantee> {
    this.logger.log(this.findOne.name);
    const item = await this.model.findOne({ _id: id });
    return item;
  }

  async remove(id: string): Promise<Grantee> {
    this.logger.log(this.remove.name);
    console.log("ID:", id);
    const result = await this.model.findOneAndDelete({ _id: id });
    return result;
  }

  async cleanData() {
    this.logger.log(this.remove.name);
    const result = await this.model.deleteMany();
    return result;
  }
}
