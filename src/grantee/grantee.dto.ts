import { QueryParam } from "../commons/custom.type";
import { ApiHideProperty, PartialType } from "@nestjs/swagger";
import {
  IsObject,
  IsOptional,
  IsString,
  Length,
  ValidateNested,
} from "class-validator";
import { IsGazetteer } from "src/commons/custom.validator";
import { Type } from "class-transformer";
export class AddressDto {
  @IsString()
  @Length(2, 2)
  province: string;

  @IsString()
  @Length(4, 4)
  district: string;

  @IsString()
  @Length(6, 6)
  commune: string;

  @IsGazetteer()
  village: string;

  street?: string;
  group?: string;
  home_number?: string;
  postal_code?: string;
}

export class CreateGranteeDto {
  @ApiHideProperty()
  beneficiary_id?: string;
  spid?: string;
  national_id: string;
  family_name_kh: string;
  given_name_kh: string;
  gender: string;
  issued_date: string;
  expired_date: string;
  date_of_birth: string;
  @ValidateNested()
  @IsObject()
  @Type(() => AddressDto)
  place_of_birth?: AddressDto;

  @IsOptional()
  @ValidateNested()
  @IsObject()
  @Type(() => AddressDto)
  current_address?: AddressDto;

  family_name_en?: string;
  given_name_en?: string;
  phone_number?: string;
  occupation?: string;
  email?: string;
  photo?: string;

  household_info?: HouseholdInfoDto;
}

export class UpdateGranteeDto extends PartialType(CreateGranteeDto) {
  status: string;
}

export class GranteeQueryDto extends PartialType(QueryParam) {}

export class HouseholdInfoDto {
  member_number: string;

  member_position: number;

  household_id: string;

  poverty_score: number;

  is_hiv_affected: boolean;

  is_covid_affected: boolean;

  equity_card_number: string;

  poverty_status: string;

  account_number: string;

  female_count: number;

  male_count: number;

  is_head: boolean;
}

export class RegistrationDto {
  national_id: string;

  beneficiary_id?: string;

  family_name_en: string;

  given_name_en: string;

  family_name_kh?: string;

  given_name_kh?: string;

  issued_date?: string;

  expired_date?: string;

  gender: string;

  date_of_birth: string;

  place_of_birth?: AddressDto;

  current_address?: AddressDto;

  occupation?: string;

  education_level?: string;

  nationality?: string;

  phone_number?: string;

  email?: string;

  photo?: string;

  operator: string;

  scheme: string;

  household_info?: HouseholdInfoDto;
}
