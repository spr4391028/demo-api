import { GranteeService } from "./grantee.service";
import { GranteeController } from "./grantee.controller";
import { Module } from "@nestjs/common";
import { Grantee, GranteeSchema } from "./grantee.schema";
import { MongooseModule } from "@nestjs/mongoose";
import { NestjsFormDataModule } from "nestjs-form-data";

import { AmqpModule } from "nestjs-amqp";
import { ConfigModule } from "@nestjs/config";

@Module({
  imports: [
    NestjsFormDataModule,
    ConfigModule.forRoot(),
    // step 1: add the connection to rabbitMQ server
    AmqpModule.forRoot({
      name: "rabbitMq",
      hostname: process.env.RMQ_HOSTNAME,
      username: process.env.RMQ_USERNAME,
      password: process.env.RMQ_PASSWORD,
      vhost: process.env.RMQ_VHOST,
      port: 5672,
    }),
    MongooseModule.forFeature([{ name: Grantee.name, schema: GranteeSchema }]),
  ],
  controllers: [GranteeController],
  providers: [GranteeService],
  exports: [GranteeService],
})
export class GranteeModule {}
