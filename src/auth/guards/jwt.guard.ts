import {
  ExecutionContext,
  HttpException,
  HttpStatus,
  Injectable,
  UnauthorizedException,
} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { TokenExpiredError } from 'jsonwebtoken';

@Injectable()
export class JwtAuthGuard extends AuthGuard('jwt') {
  canActivate(context: ExecutionContext) {
    // Add your custom authentication logic here
    // for example, call super.logIn(request) to establish a session.
    const req = context.switchToHttp().getRequest();
    var authorization: string = req.headers['authorization'];
    if (!authorization) {
      throw new TokenRequireError();
    } else {
      let token = authorization.split(' ')[1];
      if (!token) {
        throw new TokenRequireError();
      }
    }
    return super.canActivate(context);
  }

  handleRequest(err, user, info) {
    // You can throw an exception based on either "info" or "err" arguments
    if (err || !user) {
      if (info && info instanceof TokenExpiredError) {
        throw new TokenExpireError();
      }
      throw err || new CustomUnauthorizedException();
    }
    return user;
  }
}

export class TokenRequireError extends HttpException {
  constructor() {
    super(
      {
        code: -2,
        message: 'Token requires',
      },
      HttpStatus.OK,
    );
  }
}
export class TokenExpireError extends HttpException {
  constructor() {
    super(
      {
        code: -2,
        message: 'Token expired',
      },
      HttpStatus.OK,
    );
  }
}

export class CustomUnauthorizedException extends HttpException {
  constructor() {
    super(
      {
        code: -2,
        message: 'Unauthorized',
      },
      HttpStatus.OK,
    );
  }
}
