import { UserService } from "../user/user.service";
import { Body, Controller, Post, Logger } from "@nestjs/common";
import {
  ApiBearerAuth,
  ApiConsumes,
  ApiOperation,
  ApiResponse,
  ApiTags,
} from "@nestjs/swagger";
import { AuthService } from "./auth.service";
import { MResponse } from "src/commons/custom.type";
import { LoginDto } from "./auth.dto";
import { errorHandler, resHandler } from "src/utils/res.helper";

@ApiBearerAuth()
@ApiTags("auth")
@Controller()
export class AuthController {
  logger = new Logger(AuthController.name);

  constructor(
    private authService: AuthService,
    private userService: UserService
  ) {}

  @Post("/login")
  @ApiConsumes("application/json")
  @ApiOperation({ summary: "Login to system" })
  @ApiResponse({
    status: 201,
    schema: {
      example: {
        code: 1,
        data: {
          token: "",
          user: {
            _id: "",
            email: "",
            first_name: "",
            last_name: "",
          },
        },
      },
      type: MResponse.toString(),
    },
  })
  async login(@Body() login: LoginDto): Promise<MResponse> {
    this.logger.log(this.login.name);
    try {
      const result = await this.authService.loginWithEmail(
        login.email,
        login.password
      );
      return resHandler(result, this.login.name);
    } catch (error) {
      return errorHandler(error);
    }
  }
}
