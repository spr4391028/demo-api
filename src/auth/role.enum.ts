export enum Role {
  All = 'all',
  Tenant = 'tenant',
  Admin = 'admin',
  SuperAdmin = 'super_admin',
  Manager = 'manager',
  Receptionist = 'receptionist',
}
