import { Injectable, Logger, UnauthorizedException } from "@nestjs/common";
import { JwtService, JwtSignOptions } from "@nestjs/jwt";
import { CError } from "../utils/res.helper";

import { UserService } from "../user/user.service";
import * as bcrypt from "bcrypt";

@Injectable()
export class AuthService {
  private logger = new Logger(AuthService.name);
  constructor(
    private userService: UserService,
    private readonly jwtService: JwtService
  ) {}

  validateUser(username: string, password: string) {
    return { _id: "1234", username: username, password };
  }

  async validateUserById(user_id: string) {
    this.logger.log(this.validateUserById.name);
    const user = await this.userService.findOne(user_id);
    return user;
  }

  async login(user: any) {
    const payload: any = {};
    payload.sub = user._id;
    const token = this.jwtService.sign(payload);
    return {
      access_token: token,
      expires_in: process.env.ACCESS_TOKEN_EXPIRATION,
    };
  }

  async loginWithEmail(email: string, password: string) {
    this.logger.log(this.loginWithEmail.name);
    const user = await this.userService.loginWithEmail(email, password);

    const signedData: any = {};
    signedData.sub = user._id;
    const token = this.jwtService.sign(signedData);
    user.refresh_token = this.generateRefreshToken(user._id);
    await this.userService.setCurrentRefreshToken(user.refresh_token, user._id);
    return {
      token: token,
      expires_in: process.env.ACCESS_TOKEN_EXPIRATION,
      refresh_token: user.refresh_token,
      _id: user._id,
      name: user.name,
      image: user.image,
      id: user._id,
      role: user.role,
      email: user.email,
    };
  }

  generateRefreshToken(user_id: any) {
    const signedData: any = {};
    signedData.sub = user_id;
    const token = this.jwtService.sign(signedData, this.getTokenOptions());
    return token;
  }

  async createAccessTokenFromRefreshToken(refreshToken: string) {
    try {
      const decoded: any = this.jwtService.decode(refreshToken);
      if (!decoded) {
        throw new Error();
      }
      const user = await this.userService.findOne(decoded.sub);
      if (!user) {
        throw new CError("User with this id does not exist");
      }
      const isRefreshTokenMatching = await bcrypt.compare(
        refreshToken,
        user.refresh_token
      );
      if (!isRefreshTokenMatching) {
        throw new UnauthorizedException("Invalid token");
      }
      await this.jwtService.verifyAsync(
        refreshToken,
        this.getRefreshTokenOptions()
      );
      return this.login(user);
    } catch {
      throw new UnauthorizedException("Invalid token");
    }
  }

  getRefreshTokenOptions(): JwtSignOptions {
    return this.getTokenOptions();
  }

  private getTokenOptions() {
    const options: JwtSignOptions = {
      secret: process.env.REFRESH_TOKEN_SECRET,
    };
    const expiration = process.env.REFRESH_TOKEN_EXPIRATION;
    if (expiration) {
      options.expiresIn = expiration;
    }
    return options;
  }
}
