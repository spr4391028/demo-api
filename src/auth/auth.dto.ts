import { IsEmail, IsString } from 'class-validator';

export class LoginDto {
  @IsString()
  email: string;

  @IsString()
  password: string;
}

export class GetRoomAvailabilityDto {
  @IsString()
  buildingId: string;

  @IsString()
  floor: string;
}

export class GetEventActivityDto {
  date?: string;
}

export class RefreshTokenDto {
  @IsString()
  refresh_token: string;
}
