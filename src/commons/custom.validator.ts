import { Injectable } from '@nestjs/common';
import {
  registerDecorator,
  ValidationArguments,
  ValidationOptions,
  ValidatorConstraint,
  ValidatorConstraintInterface,
} from 'class-validator';

export function IsDoB(validationOptions?: ValidationOptions) {
  return function (object: any, propertyName: string) {
    registerDecorator({
      name: 'IsDoB',
      target: object.constructor,
      propertyName: propertyName,
      options: validationOptions,
      validator: IsDoBRule,
    });
  };
}

@ValidatorConstraint({ name: 'IsDoB', async: true })
@Injectable()
class IsDoBRule implements ValidatorConstraintInterface {
  async validate(value: string) {
    const regex = /^\d{4}\-(0[1-9]|1[012])\-(0[1-9]|[12][0-9]|3[01])$/;
    if (regex.test(value)) return true;
    return false;
  }

  defaultMessage(args: ValidationArguments) {
    return `${args.property} must be a date of birth (YYYY/MM/DD). `;
  }
}

export function IsGazetteer(validationOptions?: ValidationOptions) {
  return function (object: any, propertyName: string) {
    registerDecorator({
      name: 'IsGazetteer',
      target: object.constructor,
      propertyName: propertyName,
      options: validationOptions,
      validator: IsGazetteerRule,
    });
  };
}

@ValidatorConstraint({ name: 'IsDoB', async: true })
@Injectable()
class IsGazetteerRule implements ValidatorConstraintInterface {
  async validate(value: string) {
    const regex = /^[0-9]{8}$/;
    return regex.test(value);
  }

  defaultMessage(args: ValidationArguments) {
    return `${args.property} must be a Cambodian gazetteer code.`;
  }
}

export function IsGender(validationOptions?: ValidationOptions) {
  return function (object: any, propertyName: string) {
    registerDecorator({
      name: 'IsGender',
      target: object.constructor,
      propertyName: propertyName,
      options: validationOptions,
      validator: IsGenderRule,
    });
  };
}

@ValidatorConstraint({ name: 'IsGender', async: true })
@Injectable()
class IsGenderRule implements ValidatorConstraintInterface {
  async validate(value: string) {
    if (value.length == 1) {
      if (value == 'M' || value == 'F') {
        return true;
      }
    }
    return false;
  }

  defaultMessage(args: ValidationArguments) {
    return `${args.property} must be a gender code (M,F).`;
  }
}

export function IsSPID(validationOptions?: ValidationOptions) {
  return function (object: any, propertyName: string) {
    registerDecorator({
      name: 'IsSPID',
      target: object.constructor,
      propertyName: propertyName,
      options: validationOptions,
      validator: IsSPIDRule,
    });
  };
}

@ValidatorConstraint({ name: 'IsSPID', async: true })
@Injectable()
class IsSPIDRule implements ValidatorConstraintInterface {
  async validate(value: string) {
    const regex = /\d{3}-\d{3}-\d{3}-\d{1}/;
    if (regex.test(value)) return true;
    return false;
  }

  defaultMessage(args: ValidationArguments) {
    return `${args.property} must be a SPID.`;
  }
}

export function IsKhmerPhoneNumber(validationOptions?: ValidationOptions) {
  return function (object: any, propertyName: string) {
    registerDecorator({
      name: 'IsKhmerPhoneNumber',
      target: object.constructor,
      propertyName: propertyName,
      options: validationOptions,
      validator: IsKhmerPhoneNumberRule,
    });
  };
}

@ValidatorConstraint({ name: 'IsKhmerPhoneNumber', async: true })
@Injectable()
class IsKhmerPhoneNumberRule implements ValidatorConstraintInterface {
  async validate(value: string) {
    const regex =
      /\b855 *[-(]? *[0-9]{2} *[-)]? *[0-9]{3} *[-]? *[0-9]{3,4}\b|\b00855 *[-(]? *[0-9]{2} *[-)]? *[0-9]{3} *[-]? *[0-9]{3,4}\b/;
    if (regex.test(value)) return true;
    return false;
  }

  defaultMessage(args: ValidationArguments) {
    return `${args.property} must be a Cambodian phone number with country code. e.g 8550100000`;
  }
}

export function IsKhmerNationalID(validationOptions?: ValidationOptions) {
  return function (object: any, propertyName: string) {
    registerDecorator({
      name: 'IsKhmerNationalID',
      target: object.constructor,
      propertyName: propertyName,
      options: validationOptions,
      validator: IsKhmerNationalIDRule,
    });
  };
}

@ValidatorConstraint({ name: 'IsKhmerNationalID', async: true })
@Injectable()
class IsKhmerNationalIDRule implements ValidatorConstraintInterface {
  async validate(value: string) {
    const regex = /^[0-9]{9}$/;
    if (regex.test(value)) return true;
    return false;
  }

  defaultMessage(args: ValidationArguments) {
    return `${args.property} must be a Cambodian national id number. 9 digits only`;
  }
}
