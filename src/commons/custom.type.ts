import { ApiPropertyOptional } from '@nestjs/swagger';

export class MyFile {
  base64 = '';
  filename = 'filename.png';
  mimetype = 'image/jpeg';
}
export class QueryParam {
  @ApiPropertyOptional()
  count = 1000000;
  @ApiPropertyOptional()
  page = 1;
  @ApiPropertyOptional()
  search: string;
}

export enum Role {
  User = 'user',
  Admin = 'admin',
  All = 'all',
  Merchant = 'merchant',
}

export enum Scope {
  Profile = 'profile',
  FirstName = 'first_name',
  LastName = 'last_name',
  Gender = 'gender',
  IDNumber = 'id_number',
  Nationality = 'nationality',
  PlaceOfBirth = 'place_of_birth',
  DateOfBirth = 'date_of_birth',
  CurrentAddress = 'current_address',
}

export class MResponse {
  code: number;
  data: any;
  message: string;
  error: any;
}
