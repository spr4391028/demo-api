import { Controller, Logger } from "@nestjs/common";
import { ApiBearerAuth, ApiTags } from "@nestjs/swagger";

@ApiBearerAuth()
@ApiTags("app")
@Controller()
export class AppController {
  logger = new Logger(AppController.name);
}
