import { QueryParam } from "./../commons/custom.type";
import { PartialType } from "@nestjs/swagger";
export class CreateUserDto {
  name: string;
  email: string;
  password: string;
  image: string;
  role?: string = "user";
}

export class UpdateUserDto extends PartialType(CreateUserDto) {
  status: string;
}

export class UserQueryDto extends PartialType(QueryParam) {
  role?: string;
}

export class ChangePasswordDto {
  old_password: string;
  new_password: string;
}
