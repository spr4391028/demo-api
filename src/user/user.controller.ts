import { UserQueryDto, CreateUserDto, UpdateUserDto } from "./user.dto";
import {
  Body,
  Controller,
  Delete,
  Get,
  Logger,
  Param,
  Patch,
  Post,
  Query,
  UseGuards,
} from "@nestjs/common";
import {
  ApiBearerAuth,
  ApiConsumes,
  ApiOperation,
  ApiTags,
} from "@nestjs/swagger";
import { JwtAuthGuard } from "../auth/guards/jwt.guard";
import { RolesGuard } from "../auth/guards/roles.guard";
import { Role } from "../auth/role.enum";
import { UserService } from "./user.service";
import { FormDataRequest } from "nestjs-form-data";
import { MResponse } from "src/commons/custom.type";
import { errorHandler, resHandler } from "src/utils/res.helper";
import { Roles } from "src/auth/decorators/roles.decorator";

@ApiBearerAuth()
@ApiTags("users")
@Controller("users")
export class UserController {
  private logger = new Logger(UserController.name);
  constructor(private readonly userService: UserService) {}

  @Post()
  @FormDataRequest()
  @ApiConsumes("multipart/form-data")
  @ApiOperation({ summary: "Create a user" })
  @Roles(Role.Admin, Role.Manager)
  @UseGuards(JwtAuthGuard, RolesGuard)
  async createUser(@Body() data: CreateUserDto): Promise<MResponse> {
    this.logger.log(this.createUser.name);
    try {
      const result = await this.userService.create(data);
      return resHandler(result, this.createUser.name);
    } catch (error) {
      return errorHandler(error);
    }
  }

  @Get()
  @ApiOperation({ summary: "Get list of user" })
  @Roles(Role.All)
  @UseGuards(JwtAuthGuard, RolesGuard)
  async getAllUsers(@Query() query: UserQueryDto): Promise<MResponse> {
    this.logger.log(this.getAllUsers.name);
    try {
      const result = await this.userService.findAll(query);
      return resHandler(result, this.getAllUsers.name);
    } catch (error) {
      return errorHandler(error);
    }
  }

  @Get(":id")
  @ApiOperation({ summary: "Get one user" })
  @Roles(Role.All)
  @UseGuards(JwtAuthGuard, RolesGuard)
  async getOneUser(@Param("id") id: string): Promise<MResponse> {
    this.logger.log(this.getOneUser.name);
    try {
      const result = await this.userService.findOne(id);
      return resHandler(result, this.getOneUser.name);
    } catch (error) {
      return errorHandler(error);
    }
  }

  @Patch(":id")
  @FormDataRequest()
  @ApiConsumes("multipart/form-data")
  @ApiOperation({ summary: "Update a user" })
  @Roles(Role.All)
  @UseGuards(JwtAuthGuard, RolesGuard)
  async updateUser(
    @Param("id") id: string,
    @Body() body: UpdateUserDto
  ): Promise<MResponse> {
    this.logger.log(this.updateUser.name);
    // console.log('user', data);
    try {
      const result = await this.userService.update(id, body);
      return resHandler(result, this.updateUser.name);
    } catch (error) {
      return errorHandler(error);
    }
  }

  @Delete(":id")
  @ApiOperation({ summary: "Delete a user" })
  @Roles(Role.Admin, Role.Manager)
  @UseGuards(JwtAuthGuard, RolesGuard)
  async deleteUser(@Param("id") id: string): Promise<MResponse> {
    this.logger.log(this.deleteUser.name);
    try {
      const result = await this.userService.remove(id);
      return resHandler(result, this.deleteUser.name);
    } catch (error) {
      return errorHandler(error);
    }
  }
}
