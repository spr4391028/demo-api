import { UserService } from "./user.service";
import { UserController } from "./user.controller";
import { Module } from "@nestjs/common";
import { User, UserSchema } from "./user.schema";
import { MongooseModule } from "@nestjs/mongoose";
import { NestjsFormDataModule } from "nestjs-form-data";
import { ConfigModule } from "@nestjs/config";

@Module({
  imports: [
    NestjsFormDataModule,
    ConfigModule.forRoot(),
    MongooseModule.forFeature([{ name: User.name, schema: UserSchema }]),
  ],
  controllers: [UserController],
  providers: [UserService],
  exports: [UserService],
})
export class UserModule {}
