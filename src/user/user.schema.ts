import { Prop, Schema, SchemaFactory } from "@nestjs/mongoose";
import { Document } from "mongoose";

export type UserDocument = User & Document;

@Schema({ timestamps: true })
export class User {
  _id: string;

  @Prop({ type: String, default: null })
  name: string;

  @Prop({ type: String, default: null })
  email: string;

  @Prop({ type: String, default: null })
  image: string;

  @Prop({ type: String, default: null })
  password: string;

  @Prop({ type: String, default: "user", enum: ["user", "admin", "merchant"] })
  role: string;

  @Prop({ type: String, default: "active", enum: ["active", "inactive"] })
  status: string;

  @Prop({ default: null })
  refresh_token?: string;
}

export const UserSchema = SchemaFactory.createForClass(User);
