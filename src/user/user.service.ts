import { Injectable, Logger } from "@nestjs/common";
import { Model } from "mongoose";
import { CError, toResponse } from "../utils/res.helper";
import { User, UserDocument } from "./user.schema";
import * as bcrypt from "bcrypt";
import { CreateUserDto, UpdateUserDto } from "./user.dto";
import { InjectModel } from "@nestjs/mongoose";

@Injectable()
export class UserService {
  private logger = new Logger(UserService.name);

  constructor(@InjectModel(User.name) private userModel: Model<UserDocument>) {}

  async create(user: CreateUserDto) {
    this.logger.log(this.create.name);
    const salt = await bcrypt.genSalt(10);
    if (user.password) {
      const passwordHash = await bcrypt.hash(user.password, salt);
      user.password = passwordHash;
    }
    const result = await this.userModel.create(user);
    return result;
  }

  async update(id: string, user: UpdateUserDto): Promise<User> {
    this.logger.log(this.update.name);
    if (user.password) {
      const salt = await bcrypt.genSalt(10);
      const passwordHash = await bcrypt.hash(user.password, salt);
      user.password = passwordHash;
    }

    const result = await this.userModel.findOneAndUpdate(
      { _id: id },
      {
        $set: user,
      },
      {
        new: true,
      }
    );
    return result;
  }

  async findAll(arg: any): Promise<any> {
    this.logger.log(this.findAll.name);
    const { page, count, search, role } = arg;
    const query: any = {};
    if (search) {
      query.$or = [
        {
          first_name: {
            $regex: search,
            $options: "i",
          },
        },
        {
          last_name: {
            $regex: search,
            $options: "i",
          },
        },
      ];
    }
    if (role) query.role = role;
    const total_items = await this.userModel.countDocuments();
    const docs = await this.userModel.find(query).lean();
    return toResponse({ page, count, total_items, docs });
  }

  async findOne(id: string): Promise<User> {
    this.logger.log(this.findOne.name);
    const item = await this.userModel.findOne({ _id: id });
    return item;
  }

  async remove(id: string): Promise<User> {
    this.logger.log(this.remove.name);
    console.log("ID:", id);
    const result = await this.userModel.findOneAndDelete({ _id: id });
    return result;
  }

  async removeRefreshToken(id: string) {
    this.logger.log(this.removeRefreshToken.name);
    const user = await this.userModel.findOneAndUpdate(
      { _id: id },
      {
        refresh_token: null,
      }
    );
    return user == null ? false : true;
  }

  async loginWithEmail(email: string, password: string) {
    this.logger.log(this.loginWithEmail.name);
    const user = await this.userModel
      .findOne({ username: email, status: "active" })
      .lean();
    if (user) {
      const isMatch = await bcrypt.compare(password, user.password);
      if (isMatch) {
        return user;
      } else {
        throw new CError("Password is incorrect.");
      }
    } else {
      throw new CError("User not existed");
    }
  }

  async setCurrentRefreshToken(refreshToken: string, userId: string) {
    this.logger.log(this.setCurrentRefreshToken.name);
    const currentHashedRefreshToken = await bcrypt.hash(refreshToken, 10);
    return this.userModel.findOneAndUpdate(
      { _id: userId },
      {
        refresh_token: currentHashedRefreshToken,
      }
    );
  }
}
