import { GranteeModule } from "./grantee/grantee.module";
import { AuthModule } from "./auth/auth.module";
import { UserModule } from "./user/user.module";
import { Module } from "@nestjs/common";
import { MongooseModule } from "@nestjs/mongoose";
import { AppController } from "./app.controller";
import { AppService } from "./app.service";

import { ConfigModule } from "@nestjs/config";
import * as Joi from "joi";
@Module({
  imports: [
    GranteeModule,
    UserModule,
    AuthModule,
    ConfigModule.forRoot({
      validationSchema: Joi.object({
        MONGO_URI: Joi.string().required(),
        MONGO_DB_NAME: Joi.string().required(),

        ACCESS_TOKEN_EXPIRATION: Joi.number().required(),
        ACCESS_TOKEN_SECRET: Joi.string().required(),
        REFRESH_TOKEN_EXPIRATION: Joi.number().required(),
        REFRESH_TOKEN_SECRET: Joi.string().required(),

        RMQ_HOSTNAME: Joi.string().required(),
        RMQ_USERNAME: Joi.string().required(),
        RMQ_PASSWORD: Joi.string().required(),
        RMQ_VHOST: Joi.string().required(),

        REGISTRATION_QUEUE: Joi.string().required(),
        REGISTRATION_RESULT_QUEUE: Joi.string().required(),
        UPDATE_PERSON_QUEUE: Joi.string().required(),
        UPDATE_PERSON_RESULT_QUEUE: Joi.string().required(),

        SCHEME_ID: Joi.string().required(),
        OPERATOR_ID: Joi.string().required(),
      }),
      validationOptions: {
        allowUnknown: true,
        abortEarly: true,
      },
    }),
    MongooseModule.forRoot(process.env.MONGO_URI, {
      dbName: process.env.MONGO_DB_NAME,
      retryWrites: true,
      w: "majority",
      connectTimeoutMS: 60000,
    }),
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
