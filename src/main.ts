import { NestFactory } from "@nestjs/core";
import {
  DocumentBuilder,
  SwaggerCustomOptions,
  SwaggerModule,
} from "@nestjs/swagger";
import { AppModule } from "./app.module";
import * as morgan from "morgan";
import * as compression from "compression";
import { ValidationPipe } from "@nestjs/common";
import { BadRequestExceptionFilter } from "./utils/bad-request-exception.filter";
import { ForbiddenExceptionFilter } from "./utils/forbidden-exception.filter";
import { json } from "express";
async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  app.use(json({ limit: "50mb" }));
  app.setGlobalPrefix("/v1");
  const swaggerConfig = new DocumentBuilder()
    .addBearerAuth()
    .setTitle("Operator API")
    .setDescription("Operator API documentation for developer.")
    .setVersion("1.0")
    .build();
  const document = SwaggerModule.createDocument(app, swaggerConfig, {
    ignoreGlobalPrefix: false,
  });
  const customOptions: SwaggerCustomOptions = {
    swaggerOptions: {
      persistAuthorization: true,
    },
    customSiteTitle: "Operator API Docs",
  };
  SwaggerModule.setup("docs", app, document, customOptions);

  app.use(compression());
  app.use(morgan("dev"));
  app.useGlobalPipes(new ValidationPipe());
  app.enableCors();
  app.useGlobalFilters(
    new BadRequestExceptionFilter(),
    new ForbiddenExceptionFilter()
  );
  const port = 5001;
  await app.listen(port, () => {
    console.log("API is listening to port:", port);
  });
}
bootstrap();
